package br.ufrgs.inf.texttonoise;

/**
 * Command to decrease the tempo of a given conductor.
 */
public class DecreaseTempoCommand implements Command {
	public Conductor conductor;

	/**
	 * Command to decrease the tempo of a conductor
	 * 
	 * @param aConductor
	 *            A Conductor object
	 */
	public DecreaseTempoCommand(Conductor aConductor) {
		conductor = aConductor;
	}

	@Override
	public void execute() {
		conductor.decreaseTempo();
	}
}