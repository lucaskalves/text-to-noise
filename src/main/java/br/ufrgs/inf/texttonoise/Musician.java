package br.ufrgs.inf.texttonoise;

import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Synthesizer;

/**
 * This class handles the Musician.
 */
public class Musician {
	private int instrumentNumber;

	/**
	 * Constructs a new musician
	 * 
	 * @param instrument
	 *            The javasound instrument number
	 */
	public Musician(int instrument) {
		if (instrument >= 0) {
			instrumentNumber = instrument;
		} else {
			throw new IllegalArgumentException("Instrument numbers should be positive.");
		}
	}

	/**
	 * Returns the instrument number of this object
	 * 
	 * @return The instrument number
	 */
	public int getInstrumentNumber() {
		return instrumentNumber;
	}

	/**
	 * Makes this musician play a note
	 * 
	 * @param nNoteNumber
	 *            The JavaSound number of the note
	 * @param time
	 *            The time that the note will be played in ms
	 * @param volume
	 *            The volume of the note
	 */
	public void play(int nNoteNumber, int time, int volume) {

		int nChannelNumber = 1;
		int nDuration = time;
		int nVelocity = 100;

		Synthesizer synth = null;
		try {
			synth = MidiSystem.getSynthesizer();
		} catch (MidiUnavailableException e) {
			e.printStackTrace();
			System.exit(1);
		}

		/*
		 * We have to open the synthesizer to produce any sound for us.
		 */
		try {
			synth.open();
		} catch (MidiUnavailableException e) {
			e.printStackTrace();
			System.exit(1);
		}

		/*
		 * Turn the note on on MIDI channel 1. (Index zero means MIDI channel 1)
		 */
		MidiChannel[] channels = synth.getChannels();
		MidiChannel channel = channels[nChannelNumber];

		// Modify the instrument to play
		channel.programChange(getInstrumentNumber());

		// Modify the volume
		channel.controlChange(7, volume);

		// Start to play the note
		channel.noteOn(nNoteNumber, nVelocity);

		/*
		 * Wait for the specified amount of time (the duration of the note).
		 */
		try {
			Thread.sleep(nDuration);
		} catch (InterruptedException e) {
		}

		/*
		 * Turn the note off.
		 */
		channel.noteOff(nNoteNumber);

		/*
		 * Close the synthesizer.
		 */
		synth.close();

	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}

		if (!(other instanceof Musician)) {
			return false;
		}

		Musician otherMusician = (Musician) other;

		return instrumentNumber == otherMusician.getInstrumentNumber();
	}

}
