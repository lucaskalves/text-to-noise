package br.ufrgs.inf.texttonoise;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

/**
 * Writes a score to a text file.
 */
public class ScoreFileWriter {

	/**
	 * Saves the score object into this score content.
	 * 
	 * @throws IOException
	 */
	public void saveScore(Score aScore, String FilePath) throws IOException {

		Collection<Character> ScoreOutputList = aScore.getSymbols();
		FileWriter ScoreOutputFile = new FileWriter(FilePath);
		for (char aChar : ScoreOutputList) {
			ScoreOutputFile.write(aChar);
		}
		ScoreOutputFile.close();

	}

}
