package br.ufrgs.inf.texttonoise;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Read a score file and convert it to a Score object
 */
public class ScoreFileReader {
	private FileReader scoreFile;
	private Score score;
	private int beatsPerMeasure;

	public ScoreFileReader(String filepath, int beatsPerMeasure) throws FileNotFoundException {
		scoreFile = new FileReader(filepath);
		this.beatsPerMeasure = beatsPerMeasure;

		loadScore();
	}

	/**
	 * Loads the score content into this score object.
	 */
	private void loadScore() {
		score = new Score(beatsPerMeasure);
		BufferedReader buffer = new BufferedReader(scoreFile);

		try {
			String line;
			while ((line = buffer.readLine()) != null) {
				for (int i = 0; i < line.length(); i++) {
					score.addSymbol(line.charAt(i));
				}
			}
		} catch (IOException e) {
			throw new ScoreFileException("A terrible problem occured when tried to read the score file.");
		}

		try {
			scoreFile.close();
		} catch (IOException e) {
			throw new ScoreFileException("A terrible problem occured when tried to close the score file.");
		}
	}

	/**
	 * Get the score definition of the file.
	 * 
	 * @return The score object
	 */
	public Score getScore() {
		return score;
	}

}
