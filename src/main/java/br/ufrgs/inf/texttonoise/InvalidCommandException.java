package br.ufrgs.inf.texttonoise;

/**
 * Thrown when somebody tries to call an inexistent conductor command.
 */
public class InvalidCommandException extends RuntimeException {
	private static final long serialVersionUID = -7106978564887509772L;

	public InvalidCommandException() {
		super();
	}

	public InvalidCommandException(String msg) {
		super(msg);
	}

}
