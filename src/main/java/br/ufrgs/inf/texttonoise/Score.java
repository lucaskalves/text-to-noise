package br.ufrgs.inf.texttonoise;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Represents a score with a collection of symbols.
 */
public class Score {
	/**
	 * How many beats are in each measure. A simplistic Time Signature.
	 */
	public static final int defaultBeatsPerMeasure = 4;
	private int beatsPerMeasure = defaultBeatsPerMeasure;
	private List<Character> symbols = new ArrayList<Character>();

	/**
	 * Constructs a new score with a preset beats per measure quantity.
	 * 
	 * @param beatsByMeasure
	 *            Number of beats in a measure
	 */
	public Score(int beatsByMeasure) {
		this.beatsPerMeasure = beatsByMeasure;
	}

	/**
	 * Adds a symbol to the score
	 * 
	 * @param notationCode
	 *            The char code that represents the symbol
	 */
	public void addSymbol(char notationCode) {
		symbols.add(notationCode);
	}

	/**
	 * Get the collection of symbols
	 * 
	 * @return The symbols in the score
	 */
	public Collection<Character> getSymbols() {
		return symbols;
	}

	/**
	 * Get the number of beats that are inside a measure.
	 * 
	 * @return The quantity of beats per measure
	 */
	public int getBeatsPerMeasure() {
		return beatsPerMeasure;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}

		if (!(other instanceof Score)) {
			return false;
		}

		Score otherScore = (Score) other;

		return symbols.equals(otherScore.getSymbols()) && beatsPerMeasure == otherScore.getBeatsPerMeasure();
	}

	public void setBeatsPerMeasure(int beatsPerMeasure) {
		this.beatsPerMeasure = beatsPerMeasure;
	}
}
