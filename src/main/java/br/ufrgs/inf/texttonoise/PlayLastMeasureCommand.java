package br.ufrgs.inf.texttonoise;

/**
 * Command to play again the last measure played by the conductor.
 */
public class PlayLastMeasureCommand implements Command {
	public Conductor conductor;

	/**
	 * Command to play the lst measure played by the conductor
	 * 
	 * @param aConductor
	 *            A Conductor object
	 */
	public PlayLastMeasureCommand(Conductor aConductor) {
		conductor = aConductor;
	}

	@Override
	public void execute() {
		conductor.playLastMeasure();
	}
}