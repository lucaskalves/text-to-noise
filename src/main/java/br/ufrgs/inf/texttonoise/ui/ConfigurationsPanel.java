package br.ufrgs.inf.texttonoise.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.ufrgs.inf.texttonoise.presenters.ConfigurationsPresenter;

/**
 * Concrete Configurations View. Implements the view using a Swing JPanel.
 */
public class ConfigurationsPanel extends JPanel implements ConfigurationsView {

	private static final long serialVersionUID = 772800281862456628L;
	private ConfigurationsPresenter configPresenter;
	private JTextField textCurrentDicitonaryFile;
	private JButton btnChangeDictionaryFile;
	private File dictionaryFile;

	/**
	 * Create the panel.
	 */
	public ConfigurationsPanel() {
		setBackground(SystemColor.control);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		JLabel lblNotationDictionaryFile = new JLabel("Notation Dictionary File:");
		GridBagConstraints gbc_lblNotationDictionaryFile = new GridBagConstraints();
		gbc_lblNotationDictionaryFile.insets = new Insets(0, 0, 5, 5);
		gbc_lblNotationDictionaryFile.anchor = GridBagConstraints.EAST;
		gbc_lblNotationDictionaryFile.gridx = 1;
		gbc_lblNotationDictionaryFile.gridy = 1;
		add(lblNotationDictionaryFile, gbc_lblNotationDictionaryFile);

		textCurrentDicitonaryFile = new JTextField();
		textCurrentDicitonaryFile.setEnabled(false);
		textCurrentDicitonaryFile.setEditable(false);
		GridBagConstraints gbc_textCurrentDicitonaryFile = new GridBagConstraints();
		gbc_textCurrentDicitonaryFile.insets = new Insets(0, 0, 5, 0);
		gbc_textCurrentDicitonaryFile.fill = GridBagConstraints.HORIZONTAL;
		gbc_textCurrentDicitonaryFile.gridx = 2;
		gbc_textCurrentDicitonaryFile.gridy = 1;
		add(textCurrentDicitonaryFile, gbc_textCurrentDicitonaryFile);
		textCurrentDicitonaryFile.setColumns(10);

		btnChangeDictionaryFile = new JButton("Change Dictionary File");
		btnChangeDictionaryFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				changeDictionaryFile(evt);
			}
		});
		GridBagConstraints gbc_btnChangeDicionaryFile = new GridBagConstraints();
		gbc_btnChangeDicionaryFile.anchor = GridBagConstraints.WEST;
		gbc_btnChangeDicionaryFile.gridx = 2;
		gbc_btnChangeDicionaryFile.gridy = 2;
		add(btnChangeDictionaryFile, gbc_btnChangeDicionaryFile);
	}

	@Override
	public ConfigurationsPresenter getPresenter() {
		return configPresenter;
	}

	@Override
	public void setPresenter(ConfigurationsPresenter configurationsPresenter) {
		this.configPresenter = configurationsPresenter;
	}

	@Override
	public void updateModelFromView() {
		String currentFile = textCurrentDicitonaryFile.getText();
		configPresenter.getModel().setNotationDictionaryFilepath(currentFile);
	}

	@Override
	public void updateViewFromModel() {
		String currentFile = configPresenter.getModel().getNotationDictionaryFilepath();
		textCurrentDicitonaryFile.setText(currentFile);
	}

	@Override
	public File getDictionaryFile() {
		return dictionaryFile;
	}

	private void changeDictionaryFile(ActionEvent evt) {
		JFileChooser fc = new JFileChooser();
		int returnVal = fc.showOpenDialog(this);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			dictionaryFile = fc.getSelectedFile();
			getPresenter().changeNotationDictionary();
		}
	}
}
