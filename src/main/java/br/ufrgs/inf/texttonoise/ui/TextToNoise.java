package br.ufrgs.inf.texttonoise.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;

import br.ufrgs.inf.texttonoise.Conductor;
import br.ufrgs.inf.texttonoise.Configuration;
import br.ufrgs.inf.texttonoise.Musician;
import br.ufrgs.inf.texttonoise.NotationDictionary;
import br.ufrgs.inf.texttonoise.Score;
import br.ufrgs.inf.texttonoise.ScoreFileWriter;
import br.ufrgs.inf.texttonoise.presenters.ConfigurationsPresenter;
import br.ufrgs.inf.texttonoise.presenters.PlayerPresenter;

/**
 * Main class of the system. Controls the main window.
 */
public class TextToNoise {

	public static boolean FreeStyleON = false;
	public static boolean ScoreRecorderON = false;
	NotationDictionary dictionary;
	Score newScore = new Score(4);

	private JFrame frmTextToNoise;
	private static ConfigurationsView configView;
	private Configuration config;
	private ComposerView composerView;
	private PlayerView playerView;
	private JTextField txtFilename;
	private JTextPane systemMessage;
	private JSlider volumeSlider;
	private JSpinner durationSpinner;
	private Conductor conductor;
	private JButton btnSave;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					TextToNoise window = new TextToNoise();
					window.frmTextToNoise.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
					String msg = "Fatal error: \n" + e.getMessage();
					JOptionPane.showMessageDialog(null, msg);
				}
			}
		});
	}

	/**
	 * Initialize the application
	 * 
	 * @throws FileNotFoundException
	 */
	public TextToNoise() throws FileNotFoundException {
		config = new Configuration();
		initializeConfigurationsTab();
		initializePlayerTab();
		initializeMainWindow();
	}

	private void initializeConfigurationsTab() {
		ConfigurationsPresenter configPresenter = new ConfigurationsPresenter();
		configPresenter.setModel(config);
		configView = new ConfigurationsPanel();
		configPresenter.setView(configView);
		configPresenter.initialize();
	}

	private void initializePlayerTab() {
		conductor = new Conductor(config.getNotationDictionary());
		PlayerPresenter playerPresenter = new PlayerPresenter();
		playerPresenter.setConfiguration(config);
		playerPresenter.setModel(conductor);
		playerView = new PlayerPanel();
		playerPresenter.setView(playerView);
		playerPresenter.initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initializeMainWindow() {
		frmTextToNoise = new JFrame();
		frmTextToNoise.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		frmTextToNoise.setTitle("Text-To-Noise");
		frmTextToNoise.setBounds(100, 100, 550, 385);
		frmTextToNoise.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTextToNoise.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));

		JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.setBackground(Color.WHITE);
		frmTextToNoise.getContentPane().add(tabbedPane);

		JPanel playerTab = new JPanel();
		playerTab.setBackground(SystemColor.control);
		tabbedPane.addTab("Player", null, playerTab, null);
		tabbedPane.setBackgroundAt(0, Color.WHITE);
		GridBagLayout gbl_playerTab = new GridBagLayout();
		gbl_playerTab.columnWidths = new int[] { 0, 0, 0, 0, 0 };
		gbl_playerTab.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_playerTab.columnWeights = new double[] { 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_playerTab.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		playerTab.setLayout(gbl_playerTab);

		GridBagConstraints gbc_playerView = new GridBagConstraints();
		gbc_playerView.fill = GridBagConstraints.BOTH;
		gbc_playerView.gridheight = 8;
		gbc_playerView.gridy = 0;
		gbc_playerView.gridwidth = 4;
		gbc_playerView.anchor = GridBagConstraints.NORTHWEST;
		gbc_playerView.gridx = 0;
		playerTab.add((PlayerPanel) playerView, gbc_playerView);

		JPanel composerTab = new JPanel();
		tabbedPane.addTab("Composer", null, composerTab, null);
		GridBagLayout gbl_composerTab = new GridBagLayout();
		gbl_composerTab.columnWidths = new int[] { 0, 30, 30, 30, 0 };
		gbl_composerTab.rowHeights = new int[] { 0, 30, 30, 30, 30, 30, 30, 30, 0 };
		gbl_composerTab.columnWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		gbl_composerTab.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		composerTab.setLayout(gbl_composerTab);
		GridBagConstraints gbc_composerView = new GridBagConstraints();
		gbc_composerView.gridheight = 9;
		gbc_composerView.gridwidth = 4;
		gbc_composerView.fill = GridBagConstraints.BOTH;
		gbc_composerView.anchor = GridBagConstraints.NORTHEAST;
		gbc_composerView.gridx = 0;
		gbc_composerView.gridy = 0;
		composerView = new ComposerView(config.getNotationDictionary());
		composerView.setBackground(SystemColor.control);
		composerTab.add(composerView, gbc_composerView);

		JPanel freeStyleTab = new JPanel();
		tabbedPane.addTab("Free Style", null, freeStyleTab, null);
		GridBagLayout gbl_freeStyleTab = new GridBagLayout();
		gbl_freeStyleTab.columnWidths = new int[] { 0, 0 };
		gbl_freeStyleTab.rowHeights = new int[] { 0, 0 };
		gbl_freeStyleTab.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_freeStyleTab.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		freeStyleTab.setLayout(gbl_freeStyleTab);

		JPanel panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 0;
		freeStyleTab.add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0 };
		gbl_panel_1.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel_1.columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
				1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
				0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		final JButton btnStart = new JButton("Start");
		btnStart.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				FreeStyleON = false;
				systemMessage.setText("Free Style OFF");
			}
		});
		btnStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// btnStart.setEnabled(false);
				FreeStyleON = true;
				systemMessage.setText("Free Style ON");
				btnSave.setEnabled(false);

				newScore = new Score(4);

				dictionary = config.getNotationDictionary();

			}
		});

		btnStart.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {

				char charKey = e.getKeyChar();

				if (FreeStyleON == true) {

					if (dictionary.isNote(charKey) && dictionary.isInstrument(charKey)) {
						newScore.addSymbol(charKey);
						Musician aMusician = new Musician(dictionary.getInstrument(charKey));
						aMusician.play(dictionary.getNote(charKey), (int) durationSpinner.getValue(),
								volumeSlider.getValue());
					}
				}

			}
		});

		GridBagConstraints gbc_btnStart = new GridBagConstraints();
		gbc_btnStart.gridwidth = 4;
		gbc_btnStart.insets = new Insets(0, 0, 5, 5);
		gbc_btnStart.gridx = 2;
		gbc_btnStart.gridy = 3;
		panel_1.add(btnStart, gbc_btnStart);

		JButton btnStop = new JButton("Stop");
		btnStop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				btnSave.setEnabled(true);
				FreeStyleON = false;
				systemMessage.setText("Free Style OFF");

			}

		});

		GridBagConstraints gbc_btnStop = new GridBagConstraints();
		gbc_btnStop.gridwidth = 2;
		gbc_btnStop.insets = new Insets(0, 0, 5, 5);
		gbc_btnStop.gridx = 6;
		gbc_btnStop.gridy = 3;
		panel_1.add(btnStop, gbc_btnStop);

		btnSave = new JButton("Save");
		btnSave.setEnabled(false);
		btnSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String fileName = txtFilename.getText();

				try {
					String filePath = "src/test/resources/" + fileName + ".txt";
					ScoreFileWriter aWriter = new ScoreFileWriter();
					aWriter.saveScore(newScore, filePath);
					systemMessage.setText("File Saved");
				} catch (IOException e1) {
					throw new RuntimeException("Could not save the file.");
				}

			}
		});
		GridBagConstraints gbc_btnSave = new GridBagConstraints();
		gbc_btnSave.gridwidth = 2;
		gbc_btnSave.insets = new Insets(0, 0, 5, 5);
		gbc_btnSave.gridx = 6;
		gbc_btnSave.gridy = 5;
		panel_1.add(btnSave, gbc_btnSave);

		JLabel lblSeuArquivoSer = new JLabel("Your file will be saved at: \r\n\tsrc/test/resources/");
		GridBagConstraints gbc_lblSeuArquivoSer = new GridBagConstraints();
		gbc_lblSeuArquivoSer.gridwidth = 14;
		gbc_lblSeuArquivoSer.insets = new Insets(0, 0, 5, 5);
		gbc_lblSeuArquivoSer.gridx = 9;
		gbc_lblSeuArquivoSer.gridy = 5;
		panel_1.add(lblSeuArquivoSer, gbc_lblSeuArquivoSer);

		txtFilename = new JTextField();
		txtFilename.setText("FileName");
		GridBagConstraints gbc_txtFilename = new GridBagConstraints();
		gbc_txtFilename.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtFilename.gridwidth = 5;
		gbc_txtFilename.insets = new Insets(0, 0, 5, 0);
		gbc_txtFilename.gridx = 23;
		gbc_txtFilename.gridy = 5;
		panel_1.add(txtFilename, gbc_txtFilename);
		txtFilename.setColumns(10);

		JLabel lblVolume = new JLabel("Volume:");
		GridBagConstraints gbc_lblVolume = new GridBagConstraints();
		gbc_lblVolume.insets = new Insets(0, 0, 5, 5);
		gbc_lblVolume.gridx = 23;
		gbc_lblVolume.gridy = 14;
		panel_1.add(lblVolume, gbc_lblVolume);

		volumeSlider = new JSlider();
		volumeSlider.setValue(70);
		GridBagConstraints gbc_slider = new GridBagConstraints();
		gbc_slider.fill = GridBagConstraints.HORIZONTAL;
		gbc_slider.insets = new Insets(0, 0, 5, 5);
		gbc_slider.gridx = 25;
		gbc_slider.gridy = 14;
		panel_1.add(volumeSlider, gbc_slider);

		JLabel lblMensagensDoSistema = new JLabel("Mensagens do Sistema:");
		GridBagConstraints gbc_lblMensagensDoSistema = new GridBagConstraints();
		gbc_lblMensagensDoSistema.gridwidth = 8;
		gbc_lblMensagensDoSistema.insets = new Insets(0, 0, 5, 5);
		gbc_lblMensagensDoSistema.gridx = 0;
		gbc_lblMensagensDoSistema.gridy = 15;
		panel_1.add(lblMensagensDoSistema, gbc_lblMensagensDoSistema);

		systemMessage = new JTextPane();
		systemMessage.setEnabled(false);
		systemMessage.setEditable(false);
		systemMessage.setText("Free Style OFF");
		GridBagConstraints gbc_systemMessage = new GridBagConstraints();
		gbc_systemMessage.gridwidth = 9;
		gbc_systemMessage.insets = new Insets(0, 0, 5, 5);
		gbc_systemMessage.fill = GridBagConstraints.HORIZONTAL;
		gbc_systemMessage.gridx = 9;
		gbc_systemMessage.gridy = 15;
		panel_1.add(systemMessage, gbc_systemMessage);

		JLabel lblDuration = new JLabel("Note Duration:");
		GridBagConstraints gbc_lblDuration = new GridBagConstraints();
		gbc_lblDuration.insets = new Insets(0, 0, 5, 5);
		gbc_lblDuration.gridx = 23;
		gbc_lblDuration.gridy = 15;
		panel_1.add(lblDuration, gbc_lblDuration);

		durationSpinner = new JSpinner();
		durationSpinner.setModel(new SpinnerNumberModel(700, null, 1500, 1));
		GridBagConstraints gbc_durationSpinner = new GridBagConstraints();
		gbc_durationSpinner.insets = new Insets(0, 0, 5, 5);
		gbc_durationSpinner.gridx = 25;
		gbc_durationSpinner.gridy = 15;
		panel_1.add(durationSpinner, gbc_durationSpinner);

		JPanel configurationsTab = new JPanel();
		tabbedPane.addTab("Configuration", null, configurationsTab, null);
		GridBagLayout gbl_configurationsTab = new GridBagLayout();
		gbl_configurationsTab.columnWidths = new int[] { 0, 30, 30, 30, 0 };
		gbl_configurationsTab.rowHeights = new int[] { 0, 30, 30, 30, 30, 30, 30, 30, 0 };
		gbl_configurationsTab.columnWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		gbl_configurationsTab.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		configurationsTab.setLayout(gbl_configurationsTab);
		GridBagConstraints gbc_configurationsPanel = new GridBagConstraints();
		gbc_configurationsPanel.gridheight = 9;
		gbc_configurationsPanel.gridwidth = 4;
		gbc_configurationsPanel.fill = GridBagConstraints.BOTH;
		gbc_configurationsPanel.anchor = GridBagConstraints.NORTHEAST;
		gbc_configurationsPanel.gridx = 0;
		gbc_configurationsPanel.gridy = 0;
		configurationsTab.add((ConfigurationsPanel) configView, gbc_configurationsPanel);

		JPanel aboutTab = new JPanel();
		aboutTab.setBackground(Color.WHITE);
		tabbedPane.addTab("About", null, aboutTab, null);

		JTextPane txtpnTexttonoise = new JTextPane();
		txtpnTexttonoise.setEditable(false);
		txtpnTexttonoise
				.setText("Text-To-Noise\n\nA freaking awesome Text-To-Noise application!\n\nDeveloped by:\nAdolfo Henrique Schneider\nLucas josé Kreutz Alves\nRodrigo Zanella Ribeiro\n\nUniversidade Federal do Rio Grande do Sul\nInstituto de Informática\n\nNovember/2012");
		aboutTab.add(txtpnTexttonoise);
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
