package br.ufrgs.inf.texttonoise.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.ufrgs.inf.texttonoise.Musician;

public class MusicianTest {

	@Test
	public void testGetInstrumentNumber() {
		Musician aMusician = new Musician(1);
		assertEquals(1, aMusician.getInstrumentNumber());
	}

	@Test
	public void testPlay() {
		int anInstrument = 110;
		int aNote = 60;
		Musician aMusician = new Musician(anInstrument);
		aMusician.play(aNote, 1000, 100);
	}

}
