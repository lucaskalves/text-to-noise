package br.ufrgs.inf.texttonoise.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ NotationDictionaryTest.class, ScoreTest.class,
		ConductorTest.class, MusicianTest.class, ScoreFileWriterTest.class,
		ScoreFileReaderTest.class, ConductorCommandsTest.class,
		ConductorCommandsMapTest.class })
public class AllTests {
}
