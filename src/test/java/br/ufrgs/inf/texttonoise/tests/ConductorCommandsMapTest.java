package br.ufrgs.inf.texttonoise.tests;

import java.io.FileReader;
import java.io.IOException;

import org.junit.Test;

import br.ufrgs.inf.texttonoise.Conductor;
import br.ufrgs.inf.texttonoise.ConductorCommandsMap;
import br.ufrgs.inf.texttonoise.InvalidCommandException;
import br.ufrgs.inf.texttonoise.NotationDictionary;
import br.ufrgs.inf.texttonoise.Score;

public class ConductorCommandsMapTest {

	@Test(expected = InvalidCommandException.class)
	public void test() throws IOException {
		String testDictionaryFilepath = "src/test/resources/test-notes-execution-codes.json";
		FileReader dictionaryFile = new FileReader(testDictionaryFilepath);

		NotationDictionary dictionary = new NotationDictionary(dictionaryFile);
		dictionaryFile.close();

		Conductor aConductor = new Conductor(dictionary, new Score(4),
				Conductor.minTempo);

		ConductorCommandsMap commandMap = new ConductorCommandsMap(aConductor);
		commandMap.getCommand("ANonExistentCommandName");
	}
}
