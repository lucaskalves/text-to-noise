package br.ufrgs.inf.texttonoise.tests;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import br.ufrgs.inf.texttonoise.Score;
import br.ufrgs.inf.texttonoise.ScoreFileReader;
import br.ufrgs.inf.texttonoise.ScoreFileWriter;

public class ScoreFileWriterTest {

	@Test
	public void testSaveScore() throws IOException {

		String testFilePath = "src/test/resources/other-tiny-test-score.txt";

		Score expectedScore = new Score(4);
		expectedScore.addSymbol('A');
		expectedScore.addSymbol('A');
		expectedScore.addSymbol('A');
		expectedScore.addSymbol('A');

		ScoreFileWriter aWriter = new ScoreFileWriter();
		aWriter.saveScore(expectedScore, testFilePath);

		ScoreFileReader savedScore = new ScoreFileReader(testFilePath, 4);

		assertEquals(expectedScore, savedScore.getScore());
	}

}
