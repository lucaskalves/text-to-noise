# Text-to-Noise

An awesome text-to-noise application. Yep, Noise, not Speech.
It gets a boring and tedious text and converts it to an
awesome undefined sound. Pretty cool, no?

## How to develop in Eclipse

Just clone this code in your Eclipse workspace and import this
project to your Eclipse instance.

> ### Observation
>
> You can use any IDE you want to use. The code is in the
> src directory. Netbeans, for example, supports Eclipse
> projects importation.

## How to run the application using Eclipse

After importing the project, just run the class *TextToNoise* present in
the package *br.ufrgs.inf.texttonoise.ui*.

You can play any file in the Player tab. Just load it and press the *Play* button.
You can change the dictionary in the *Configurations* tab. There are some dictionaries
examples (JSON files) in the *sample-files* directory. There are more test dictionaries
inside the *src/test/resources* directory.

In the *Composer* tab you can type a score and play it directly in the interface.

In the *Freestyle* tab you can act like a *Conductor*. Just press the *start* button and start
pressing the buttons of your keyboard. Just remember to press the keys that are mapped in the
dictionary definition file.

## Requirements

* Java JDK 1.7
* [JUnit 4](http://www.junit.org/)
* [json-simple 1.1.1](http://code.google.com/p/json-simple/) (jar included in the source)

## Recommendations

* [Eclipse Juno IDE](http://www.eclipse.org/downloads/packages/eclipse-ide-java-developers/junor)

## Authors

* Adolfo Henrique Schneider
* Lucas Jose Kreutz Alves - [lucaskreutz.com.br](http://www.lucaskreutz.com.br)
* Rodrigo Zanella Ribeiro

## License

The good and old MIT License. See license.txt file.
